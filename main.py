from flask import Flask, redirect, url_for, request, render_template
import random
app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    return render_template("index.html")


@app.route('/search', methods=['GET'])
def get_search():
    return render_template("get_artist.html")


@app.route('/search', methods=['POST'])
def search():
    artist = request.form['nm']
    result = {1234: "A Sense of Gravity", 4321: "A Smense of Smavity"}
    return render_template("artist_search_results.html", result=result)


@app.route('/artists/', methods=['POST'])
def add_artist():
    artist = request.form['nm']
    artist_id = random.randrange(1000, 1999)
    return redirect(url_for('get_artist', artist_id=artist_id))


@app.route('/artists/', methods=['GET'])
def get_add_artist():
    return render_template("add_artist.html")


@app.route('/artists/<int:artist_id>/', methods=['GET'])
def get_artist(artist_id):
    return redirect(url_for('get_artist_version', artist_id=artist_id, artist_vrs=1.0))


@app.route('/artists/<int:artist_id>/<float:artist_vrs>', methods=['GET'])
def get_artist_version(artist_id, artist_vrs):
    return render_template("artist_page.html", name="A Sense of Gravity", id=artist_id, version=artist_vrs)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    app.run(debug=True)
